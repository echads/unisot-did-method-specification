# UNISOT DID Method Specification

Mirko Stanić, UNISOT AS

November 25th, 2020

# 1. Introduction

The UNISOT distributed identity system aims to provide a secure, robust and flexible implementation of the DID and Verifiable Claims specifications published by the W3C and the Decentralised Identity Foundation. It’s core technologies are the Bitcoin SV blockchain which is public and permissionless.

This document is draft of a potential specification. It has no official standing of any kind and does not represent the support or consensus of any standards organization.

# 2. Overview

The UNISOT DID method uses the Bitcoin SV blockchain to generate DIDs as well as potentially store the associated DID documents. The method allows for storage of DID documents on-chain as well as off-chain depending on the business use case scenario.

# 3. Specification

The following section outlines the DID operations for the did:unisot method. Since did:unisot values are not stored in any registry, they cannot be updated or deactivated in classical ways and updating and deleting are therefore defined on a protocol level as further explained in their respective sections.

UNISOT DIDs are identifiable by the `did:unisot:` method string and conform to the [DID Scheme](https://www.w3.org/TR/did-core).

```
unisot-did = "did:unisot:" unisot-specific-idstring
unisot-specific-idstring = [ unisot-network ":" ] unisot-address
unisot-network = "mainnet"/"testnet"
unisot-address = Bitcoin SV address
```

In case on mainnet DIDs the unisot-network parameter should be omitted.

#### Example of UNISOT DIDs:

```
did:unisot:1A2LnUwQAL5Na5iz1xcD7DYcwu4cGy6MLP
did:unisot:mainnet:16WGGXU6gXfvCJrPRqF9tqFZHQ9YRHuQfA
did:unisot:testnet:mhzuR9LJuJvwq9hfYLpgtvxuwyAMvNsPwG
```

### 3.1 DID Registration (Create)

In order to create a UNIOST DID on the Bitcoin SV blockchain the following steps must be taken:

1. Generate a valid Bitcoin SV key pair
2. From the Bitcoin SV key pair, take the address of the associated public key. This address serves as the DID.
3. Perform a transaction on the Bitcoin SV blockchain from the address identified by the DID and write the DID document of said DID in the OP_RETURN field of the transaction.

#### Example of a UNISOT DID document:

```
{
  "@context": "https://www.w3.org/ns/did/v1",
  "id": "did:unisot:1A2LnUwQAL5Na5iz1xcD7DYcwu4cGy6MLP",
  "publicKey": [{
    "id": "did:unisot:1CFPke8f4Bs611FnTASrVvJsugSbZsvqdz#key1",
    "type": "EcdsaSecp256k1VerificationKey2019",
    "controller": "did:unisot:1NF4xwFj8tJEv3XGjMNzfktaRJVFbCVBLX",
    "publicKeyBase58": "KmoEcdMASm6A3eNPdLkwQmwtKo...CFG6LCqJN2UC8avj2TwCR6S"
  }],
  "authentication": [
    "did:unisot:1G1z5U7XTBFLRPx3q5YUT6XMv7XftQ5dq9#key1"
  ],
  "service": [{
    "id": "did:unisot:1F7jWegWAjbamP9R3TZgJsDQ41x41RC4R2#service",
    "type": "VerifiableCredentialService",
    "serviceEndpoint": "https://service.example.com/vc"
  }]
}
```

### 3.2 DID Resolution (Read)

Because Bitcoin SV allows for the OP_RETURN field to be up to 100 KB in size, UNISOT DID documents can be stored directly on the chain mitigating the need for external storage. The DID document can also be stored off-chain which is a use case that will be defined in the future and can be domain specific. DID Document resolution is achieved by reading the OP_RETURN field of the last valid transaction done with the specified DID identifier.

### 3.3 DID Document Updating (Update)

Updating a UNISOT DID document is performed by signing a new transaction using the DID address as the input and storing an updated version of the DID document in the OP_RETURN field.

### 3.4 DID Document Deleting (Delete)

Deleting a DID is implemented on a protocol level by writing a flag in the DID document which signals that the document is deleted and burning said address. This way the intent of the burn is made clear and also the DID cannot be recovered in the future. Temporary Revocation can also be performed by writing a flag status in the updated DID document but without burning said DID.

# 4. Security Requirements

Performing a transaction on the Bitcoin SV blockchain requires control over the private key used to anchor the DID, therefore any key recovery method which applies to Bitcoin SV keys can be applied here. This can involve seed phrases stored in an analogue manner among other methods.

The person/entity which controls the DID private key also controls the DID document and therefore great care must be taken to ensure the safety of the DID private key. Such methods for ensuring key security are outside the scope of this document.

# 5. Privacy Requirements

UNISOT DID method allows for the storing of DID documents on-chain in order to comply with [eIDAS](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32014R0910&from=HR) regulation and provide compatibility with the European Self-Sovereign Identity Framework [ESSIF](https://ec.europa.eu/cefdigital/wiki/pages/viewpage.action?pageId=262505360).

For use cases which do not have these requirements off-chain storage is also supported, in which case a DID resolver is used to match the DID to its document. The [GDPR](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=HR) directive protects pseudonymized data stored on the blockchain because of the "linkability" of an unreadable hash.

As registered DIDs can be resolved by anyone and are written in the OP_RETURN field, care must be taken to only store data which does not expose any personal information whatsoever.

# 6. Sample implementations

[UNISOT DID Resolver](https://gitlab.com/unisot-did/unisot-did-resolver)

[UNISOT DID Driver](https://gitlab.com/unisot-did/unisot-did-driver)